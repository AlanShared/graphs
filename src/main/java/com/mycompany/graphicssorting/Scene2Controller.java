package com.mycompany.graphicssorting;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static java.lang.Thread.sleep;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;


/**
 * FXML Controller class
 *
 * @author CinthyaLiliana
 */
public class Scene2Controller implements Initializable {

//    int maxSize = 10;
//    ArraysClass a = new ArraysClass(maxSize);

    final static String position1 = "1";
    final static String position2 = "2";
    final static String position3 = "3";
    final static String position4 = "4";
    final static String position5 = "5";
    final static String position6 = "6";
    final static String position7 = "7";
    final static String position8 = "8";
    final static String position9 = "9";
    final static String position10 = "10";

    @FXML
    private void grafica() {
        
        Stage stage = new Stage();
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final StackedBarChart<String, Number> sbc = new StackedBarChart<String, Number>(xAxis, yAxis);
        final XYChart.Series<String, Number> series1 = new XYChart.Series<String, Number>();

        stage.setTitle("Tiempos");
        sbc.setTitle("Bubble Sort");
        xAxis.setLabel("Positions");

        xAxis.setCategories(FXCollections.<String>observableArrayList(
                Arrays.asList(position1, position2, position3, position4, position5, position6, position7, position8, position9, position10)));
        yAxis.setLabel("Values");
       
        int arr[] = new int[10];

        for (int i = 0; i < 10; i++) {

            arr[i] = (int) (Math.random() * 999);
        }

        series1.getData().add(new XYChart.Data<String, Number>(position1, arr[0]));
        series1.getData().add(new XYChart.Data<String, Number>(position2, arr[1]));
        series1.getData().add(new XYChart.Data<String, Number>(position3, arr[2]));
        series1.getData().add(new XYChart.Data<String, Number>(position4, arr[3]));
        series1.getData().add(new XYChart.Data<String, Number>(position5, arr[4]));
        series1.getData().add(new XYChart.Data<String, Number>(position6, arr[5]));
        series1.getData().add(new XYChart.Data<String, Number>(position7, arr[6]));
        series1.getData().add(new XYChart.Data<String, Number>(position8, arr[7]));
        series1.getData().add(new XYChart.Data<String, Number>(position9, arr[8]));
        series1.getData().add(new XYChart.Data<String, Number>(position10, arr[9]));
       
        
        Scene scene = new Scene(sbc, 800, 600);
        sbc.getData().addAll(series1);
        stage.setScene(scene);
        stage.show();
        
//        try {
//            sleep(3000);
//            System.out.println("Entra a sleep");
//        } catch (InterruptedException ex) {
//            System.out.println("entro a catch");
//            Logger.getLogger(Scene2Controller.class.getName()).log(Level.SEVERE, null, ex);
//        }

//        
//        try {
//            series1.wait(3000);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(Scene2Controller.class.getName()).log(Level.SEVERE, null, ex);
//        }
       
        series1.getData().add(0,new XYChart.Data<String, Number>(position1, arr[1]));
        System.out.println("limpia datos");

//        stage.show();
   
        

    }

    @FXML
    private BarChart graficaChart;

    @FXML
    private void graficar(ActionEvent e) {

//        graficaChart.setTitle("Bubble");
//        
        grafica();

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }
}
